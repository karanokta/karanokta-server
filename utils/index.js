import fetch from 'node-fetch';

export const getGeocode = async (latitude, longitude) => {
  return await fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}`)
    .then(res => res.json())
    .then(({results}) => results)
}
