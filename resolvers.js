import Sequelize from 'sequelize';

import config from './config';
import  { getGeocode } from './utils'
const { host, db_name, username, password } = config;

const sequelize = new Sequelize(
  db_name, username, password, 
  {
    host,
    dialect: 'mysql',
    dialectOptions: {
      charset: "utf8_general_ci"
    },
    define: {
      charset: 'utf8',
      collate: 'utf8_general_ci', 
      timestamps: true
    }
  }
);

export default {
  Point: {},
  Query: {
    getPoints: (parent, args, {models}) => models.Point.findAll(),
    getPointDetail: async (parent, {latitude, longitude}, {models}) => {
      var address = await getGeocode(latitude, longitude);
      return {
        latitude,
        longitude,
        address: address.length ? address[0].formatted_address : null
      }
      
    }
  },
  Mutation: {
    createPoint: (parent, args, {models}) => models.Point.create(args, {raw: true}) 
  },
}
