export default (sequelize, DataTypes) => {
  const Point = sequelize.define('Point', {
    title: {
      type: DataTypes.STRING,
    },
    latitude: DataTypes.FLOAT,
    longitude: DataTypes.FLOAT,
    description: DataTypes.STRING(2555),
  },{
    charset: 'utf8', collate: 'utf8_unicode_ci'
  });
  return Point;
};
