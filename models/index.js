import Sequelize from 'sequelize';

import config from '../config';
const { host, db_name, username, password } = config;

const sequelize = new Sequelize(
  db_name, username, password, 
  {
    host,
    dialect: 'mysql',
    dialectOptions: {
      charset: "utf8_general_ci"
    },
    define: {
      charset: 'utf8',
      collate: 'utf8_general_ci', 
      timestamps: true
    }
  }
);

const db = {
  Point: sequelize.import('./Point'),
}

Object.keys(db).forEach((modelName) => {
  if("associate" in db[modelName]) {
    db[modelName].associate(db)
  }
});

db.sequelize = sequelize;

export default db;
