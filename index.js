import express from 'express';
import bodyParser from 'body-parser';

import { graphiqlExpress, graphqlExpress } from 'graphql-server-express';
import { makeExecutableSchema } from 'graphql-tools';
import { execute, subscribe } from 'graphql';
import { SubscriptionServer } from 'subscriptions-transport-ws';

import { createServer } from 'http';
import cors from 'cors';

import typeDefs from './schema';
import resolvers from './resolvers';
import models from './models';

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

const app = express();

app.use(
  '/graphiql',
  graphiqlExpress({
    endpointURL: '/graphql',
  }),
);

app.use(
  '/graphql',
  cors(),
  bodyParser.json(),
  graphqlExpress(req => ({ schema, context: {models} })),
);

const server = createServer(app);

models.sequelize.sync().then(() => 
    server.listen(8080)
)
