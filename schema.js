export default `
  type Point{
    id: Int!
    title: String!
    latitude: Float!
    longitude: Float!
    description: String!
  }

  type PointDetail{
    latitude: Float!
    longitude: Float!
    address: String
  }
  
  type Query {
    getPoints: [Point]
      getPointDetail(latitude: Float!, longitude: Float!): PointDetail!
  }

  type Mutation {
    createPoint(title: String!, latitude: Float!, longitude: Float!, description: String!): Point!
  }

  schema{
    query: Query
    mutation: Mutation
  }
`;
